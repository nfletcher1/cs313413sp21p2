TEST ITERATOR:

also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
no test results change, so not in any way that we are measuring

what happens if you use list.remove(Integer.valueOf(77))?
the test fails (given the fixed parameters), presumably because list.remove(Integer.valueOf(77))
only removes the first instance of 77 in the list, so the resulting list is [33, 44, 77, 55, 77, 66]


TEST LIST:

also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
no test results change, so not in any way that we are measuring

list.remove(5); // answer: what does this method do?
removes the element at index 5

list.remove(Integer.valueOf(5)); // answer: what does this one do?
removes the element with a value of 5

TEST PERFORMANCE:

size 10:
testArrayListAccess	    0.038s	passed
testArrayListAddRemove	0.063s	passed
testLinkedListAccess	0.020s	passed
testLinkedListAddRemove	0.070s	passed

size 100:
testArrayListAccess	    0.044s	passed
testArrayListAddRemove	0.073s	passed
testLinkedListAccess	0.034s	passed
testLinkedListAddRemove	0.054s	passed

size 1000:
testArrayListAccess	    0.036s	passed
testArrayListAddRemove	0.259s	passed
testLinkedListAccess	0.434s	passed
testLinkedListAddRemove	0.064s	passed

size 10,000:
testArrayListAccess	    0.048s	passed
testArrayListAddRemove	2.210s	passed
testLinkedListAccess	6.355s	passed
testLinkedListAddRemove	0.061s	passed

test 100,000:
testArrayListAccess	    0.076s	    passed
testArrayListAddRemove	37.548s	    passed
testLinkedListAccess	1m14.57s    passed
testLinkedListAddRemove	0.054s	    passed

ArrayLists are faster for access as size increases
LinkedLists are faster for adding and removing as size increases

